# Pan1c-Apps - Apptainer images for Pan1c workflow

Repo containing some apptainer images used in [Pan1c workflow](https://forgemia.inra.fr/alexis.mergez/pan1c).

## pan1c-box
This image wraps snakemake and apptainer for the Pan1c workflow. This is the main images as it runs the worflow.  
To use it : 
```
# Generic
apptainer run pan1c-box.sif <command>
# Snakemake
apptainer run pan1c-box.sif snakemake <snakemake_command>
``` 
> The container entrypoint with `run` is : `micromamba run -p /apps/base <command>`.  

## pan1c-env
This image wraps the conda environment used in the Pan1c workflow. 
The command to use it is similar to `pan1c-box`.



